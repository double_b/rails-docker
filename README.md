### Example Rails Repo

This repo consists of the following:  
- Rails App  
- Redis  
- Sidekiq
- RPC Server
- WebSocket Server
    
All of this services are run as separate containers.  

Rails, Sidekiq and RPC server use the same image which is built using the Dockerfile.

---  

#### Usage

You can run all services using docker-compose:

```bash
docker-compose up
``` 

---

There is also an example **Dockerrun.aws.json** file generated from **docker-compose.yml**.  
To be able to run this setup on Elastic Beanstalk you need:  
1. Build image from Dockerfile and push it to your registry.
2. Edit the **Dockerrun.aws.json** replacing `$rails_image` string with corresponding image from your registry.

Note: EB version is not tested so maybe there may be need additional changes.
    

