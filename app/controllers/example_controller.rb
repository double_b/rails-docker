class ExampleController < ApplicationController

  def trigger_sidekiq
    HardWorker.perform_async
  end
end
