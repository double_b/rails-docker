Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  get '/trigger_sidekiq', to: 'example#trigger_sidekiq'
end
